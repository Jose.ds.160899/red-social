<?php

use Illuminate\Support\Facades\Route;

Route::view('/', 'welcome')->name('home');

// statuses routes
Route::get('statuses','StatusesController@index')->name('statuses.index');
Route::post('statuses','StatusesController@store')->name('statuses.store')->middleware('auth');

// statuses Likes routes

Route::post('statuses/{status}/likes','StatusLikesController@store')->name('statuses.likes.store')->middleware('auth');
Route::delete('statuses/{status}/likes','StatusLikesController@destroy')->name('statuses.likes.destroy')->middleware('auth');

// Statuses comments routes

Route::post('statuses/{status}/comments','StatusCommentsController@store')->name('statuses.comments.store')->middleware('auth');


// comments likes routes

Route::post('comments/{comment}/likes','CommentLikesController@store')->name('comments.likes.store')->middleware('auth');
Route::delete('comments/{comment}/likes','CommentLikesController@destroy')->name('comments.likes.destroy')->middleware('auth');

// user routes

Route::get('@{user}','UsersController@show')->name('users.show');

// auth
Route::auth();


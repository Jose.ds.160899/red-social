<?php

namespace App\Http\Controllers;

use App\Models\Status;
use App\Http\Resources\StatusResource;
use Illuminate\Http\Request;

class StatusesController extends Controller
{
    public function index(){
        // dd('Aqui');
        return StatusResource::collection(
            Status::latest()->paginate()
        );
    }
    public function store(Request $request){
        $request->validate(['body' => 'required|min:5']);

        $status = Status::create([
            'body' => $request->body,
            'user_id' => auth()->id()
        ]);

        // return $status->body->toJson();
        return StatusResource::make($status);
    }
}

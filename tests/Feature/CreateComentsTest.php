<?php

namespace Tests\Feature;

use App\User;
use App\Models\Status;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreateComentsTest extends TestCase
{
    use RefreshDatabase;
    /** @test */
    public function guest_users_can_coments_statuses()
    {
        $status = factory(Status::class)->create();
        $comment = ['body' => 'mi primer comentario'];
        $response = $this->postJson(route('statuses.comments.store',$status),$comment);

        $response->assertStatus(401);

    }

    /** @test */
    public function authenticated_users_can_coments_statuses()
    {
        $this->withoutExceptionHandling();
        $status = factory(Status::class)->create();
        $user = factory(User::class)->create();
        $comment = ['body' => 'mi primer comentario'];
        $response = $this->actingAs($user)->postJson(route('statuses.comments.store',$status),$comment);

        $response->assertJson([
            'data' => ['body' => $comment['body']]
        ]);
        $this->assertDatabaseHas('comments',[
            'user_id' => $user->id,
            'status_id' => $status->id,
            'body' => $comment['body']
        ]);

    }

    /** @test */
    public function a_comment_requires_a_body(){

        $status = factory(Status::class)->create();
        $user = factory(User::class)->create();

        $this->actingAs($user);

        $response = $this->postJson(route('statuses.comments.store',$status),['body' => '']);

        $response->assertStatus(422);

        $response->assertJsonStructure([
            'message', 'errors' => ['body']
        ]);
    }
}

<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreateStatusTest extends TestCase
{
    use RefreshDatabase;
     /** @test */
    function guests_users_can_not_create_statuses(){
        // $this->withoutExceptionHandling();
        $response = $this->postJson(route('statuses.store'),["body" => "Mi Primer estado"]);

        $response->assertStatus(401);
    }

    /** @test */
    public function a_authenticated_user_can_create_statuses()
    {
        $this->withoutExceptionHandling();
        // 1. Given => Teniendo - contextos (teniendo un usuario autenticado)
        $user = factory(User::class)->create();
        $this->actingAs($user);
        // 2. When => Cuando (cuando se hace un post request a status)

        $response = $this->postJson(route('statuses.store'),["body" => "Mi Primer estado"]);

        $response->assertJson([
            'data' => ['body'=>'Mi Primer estado']
        ]);
        // 3. Then => Entonces (Entonces veo un nuevo estado en la bd)
        // $response = $this->get('/');
        $this->assertDatabaseHas('statuses',[
            'user_id' => $user->id,
            'body' => 'Mi Primer estado'
        ]);
        // $response->assertStatus(200);
    }

    /** @test */
    public function a_status_requires_a_body(){
        // $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $response = $this->postJson(route('statuses.store'),["body" => ""]);
        // dd($response->getContent());
        // $response->assertSessionHasErrors('body');esto es para verificar cuando se redirecciona

        $response->assertStatus(422);

        $response->assertJsonStructure([
            'message','errors' => ['body']
        ]);
    }

    /** @test */
    public function a_status_body_requires_a_minimum_length(){
        // $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $response = $this->postJson(route('statuses.store'),["body" => "1234"]);
        // dd($response->getContent());
        // $response->assertSessionHasErrors('body');esto es para verificar cuando se redirecciona

        $response->assertStatus(422);

        $response->assertJsonStructure([
            'message','errors' => ['body']
        ]);
    }
}

<?php

namespace Tests\Feature;

use App\User;
use App\Models\Comment;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CanLikeCommentsTest extends TestCase
{
    use RefreshDatabase;
     /** @test */
     public function an_users_guest_can_not_like_a_comments()
     {
        //  $this->withoutExceptionHandling();

        $comment = factory(Comment::class)->create();

        $response = $this->postJson(route('comments.likes.store',$comment));

        $response->assertStatus(401);
     }
     /** @test */
    public function an_authenticated_user_can_like_and_unlike_comments()
    {
        // $this->markTestIncomplete();

        $this->withoutExceptionHandling();

        $user = factory(User::class)->create();
        $comment = factory(Comment::class)->create();

        $this->assertCount(0 ,$comment->likes);

        $this->actingAs($user)->postJson(route('comments.likes.store',$comment));

        $this->assertCount(1 ,$comment->fresh()->likes);

        $this->assertDatabaseHas('likes',['user_id' => $user->id]);


        $this->actingAs($user)->deleteJson(route('comments.likes.destroy',$comment));

        $this->assertCount(0 ,$comment->fresh()->likes);

        $this->assertDatabaseMissing('likes',['user_id' => $user->id]);
    }
}

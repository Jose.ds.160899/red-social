<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\RefreshDatabase;


class RegistrationTest extends TestCase
{
    use RefreshDatabase;
    /** @test */
    function users_can_register()
    {
        $this->withoutExceptionHandling();

        $response = $this->post(route('register',$this->userValidData()));

        $response->assertRedirect('/');

        $this->assertDatabaseHas('users',[
            'name' => 'Jose',
            'first_name' => 'Jose',
            'last_name' => 'Tenorio',
            'email' => 'jose.ds.160899@gmail.com',
        ]);

        $this->assertTrue(
            Hash::check('password',User::first()->password),
            'The password needs to be hashed'
        );
    }

    /** @test */
    function the_name_is_required(){
        $this->post(
            route('register'),
            $this->userValidData(['name' => null])
        )->assertSessionHasErrors('name');
    }

    /** @test */
    function the_name_must_be_string(){
        $this->post(
            route('register'),
            $this->userValidData(['name' => 1234])
        )->assertSessionHasErrors('name');
    }

    /** @test */
    function the_name_may_not_be_greater_than_60_characters(){
        $this->post(
            route('register'),
            $this->userValidData(['name' => Str::random(61)])
        )->assertSessionHasErrors('name');
    }

    /** @test */
    function first_name_is_required(){
        $this->post(route('register'), $this->userValidData(['first_name' => null]))->assertSessionHasErrors('first_name');
    }

    /** @test */
    function the_first_name_must_be_string(){
        $this->post(
            route('register'),
            $this->userValidData(['first_name' => 1234])
        )->assertSessionHasErrors('first_name');
    }

    /** @test */
    function the_first_name_may_not_be_greater_than_60_characters(){
        $this->post(
            route('register'),
            $this->userValidData(['first_name' => Str::random(61)])
        )->assertSessionHasErrors('first_name');
    }

    /** @test */
    function last_name_is_required(){
        $this->post(route('register'), $this->userValidData(['last_name' => null]))->assertSessionHasErrors('last_name');
    }

    /** @test */
    function the_username_must_be_unique() {
        factory(User::class)->create(['name' => 'joseds']);

        $this->post(
            route('register'),
            $this->userValidData(['name' => 'joseds'])
            )->assertSessionHasErrors('name');
    }

    /** @test */
    function the_username_name_is_required() {
        factory(User::class)->create(['name' => 'joseds']);

        $this->post(
            route('register'),
            $this->userValidData(['name' => 'joseds'])
            )->assertSessionHasErrors('name');
    }
    /** @test */
    function the_last_name_must_be_string(){
        $this->post(
            route('register'),
            $this->userValidData(['last_name' => 1234])
        )->assertSessionHasErrors('last_name');
    }

    /** @test */
    function the_last_name_may_not_be_greater_than_60_characters(){
        $this->post(
            route('register'),
            $this->userValidData(['last_name' => Str::random(61)])
        )->assertSessionHasErrors('last_name');
    }

    /** @test */
    function the_email_must_be_string(){
        $this->post(
            route('register'),
            $this->userValidData(['email' => 1234])
        )->assertSessionHasErrors('email');
    }


    /** @test */
    function the_email_must_be_a_valid_email_address() {
        $this->post(
            route('register'),
            $this->userValidData(['email' => 'invalid@'])
            )->assertSessionHasErrors('email');
    }

    /** @test */
    function the_email_may_not_be_greater_than_100_characters(){
        $this->post(route('register'), $this->userValidData(['email' => Str::random(101)]))->assertSessionHasErrors('email');
    }

    /** @test */
    function the_email_must_be_unique() {
        factory(User::class)->create(['email' => 'jose.ds.160899@gmail.com']);

        $this->post(
            route('register'),
            $this->userValidData(['email' => 'jose.ds.160899@gmail.com'])
            )->assertSessionHasErrors('email');
    }

    /** @test */
    function the_password_is_required(){
        $this->post(route('register'),$this->userValidData(['password' => null]))
            ->assertSessionHasErrors('password');
    }

    /** @test */
    function the_password_must_be_a_string(){
        $this->post(route('register'),$this->userValidData(['password' => 1234]))
            ->assertSessionHasErrors('password');
    }

    /** @test */
    function the_password_must_be_at_least_6_characters(){
        $this->post(route('register'), $this->userValidData(['password' => '12345']))->assertSessionHasErrors('password');
    }

    /** @test */
    function the_password_must_be_confirmed(){
        $this->post(route('register'), $this->userValidData([
            'password' => 'password',
            'password_confirmation' => null
        ]))->assertSessionHasErrors('password');
    }


    /**
    * @return array
    */
     protected function userValidData($overrides = []) : array {
        return array_merge([
            'name' => 'Jose',
            'first_name' => 'Jose',
            'last_name' => 'Tenorio',
            'email' => 'jose.ds.160899@gmail.com',
            'password' => 'password',
            'password_confirmation' => 'password'
        ],$overrides);
     }
}

<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function assertClassUsesTraits($trait, $class){
        $this->assertArrayHasKey(
            $trait,
            class_uses($class),
            "La clase {$class} debe el traits {$trait}"
        );
    }
}
